import { RealprojectPage } from './app.po';

describe('realproject App', () => {
  let page: RealprojectPage;

  beforeEach(() => {
    page = new RealprojectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
